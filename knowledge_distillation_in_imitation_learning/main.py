import torch
import numpy as np
import gymnasium as gym
from utils.env_utils import run_episode
# Example usage
env = gym.make("ALE/KungFuMaster-v5", render_mode="human", obs_type="rgb")
policy = lambda observation: np.random.randint(4)
episodes = 10
for episode in range(episodes):
    reward = run_episode(env, policy)
    print(f"Total reward: {reward}")
env.close()


