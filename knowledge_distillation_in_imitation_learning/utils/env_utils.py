import numpy as np

def run_episode(env, policy, render=False):
  """Runs a single episode of an environment using a given policy.
  
  Args:
    env: The gym environment to run the episode in.
    policy: A function that takes in an observation and returns an action.
    render: A boolean indicating whether to render the environment.
  
  Returns:
    The total reward obtained in the episode.
  """
  observation = env.reset()
  ram_observation = None
  done = False
  total_reward = 0
  while not done:
    if render:
      env.render()
    action = policy(observation)
    observation, reward, done, _,_ = env.step(action)
    ram_observation = env.unwrapped.ale.getRAM()

    total_reward += reward
  return total_reward